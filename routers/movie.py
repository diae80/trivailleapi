from fastapi import APIRouter, Path, Query, Depends
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from config.database import Session
from services.movie import MovieService
from schemas.movie import Movie
from typing import List

movie_router = APIRouter()

@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200)
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies/{id}', tags=['movies'])
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "La película no ha sido encontrada"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))
   
@movie_router.get('/movies/', tags=['movies'])
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies_by_category(category)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.post('/movies/', tags=['movies'], response_model=List[Movie], status_code=200)
def create_movie(movie: Movie) -> dict:
    db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code=200, content={"message": "La película ha sido registrada correctamente"})

@movie_router.put('/movies/{id}', tags=['movies'], response_model=List[Movie], status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "La película no ha sido encontrada"})
    MovieService(db).update_movie(id, movie)
    return JSONResponse(status_code=200, content={"message": "La película ha sido actualizada correctamente"})

@movie_router.delete('/movies/{id}', tags=['movies'], response_model=List[Movie], status_code=200)
def delete_movie(id: int) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "La película no ha sido encontrada"})
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=200, content={"message": "La película ha sido eliminada correctamente"})
