from fastapi import FastAPI
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel, Field
from config.database import engine, Base, Session
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from models.computadoras import Computadoras as ComputadorasModel
from typing import List, Optional
from routers.user import user_router

app = FastAPI()
app.title = "API para la gestión de películas y computadoras"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)

# MENSAJE

@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>!Bienvenido a computadoras en linea!</h1>')


class Computadoras(BaseModel):
    id: Optional[int] = None
    marca: str = Field(default="Marca de Computadora", min_length=2, max_length=50)
    modelo: str = Field(default="Modelo de Computadora", min_length=2, max_length=50)
    color: str = Field(default="Color de Computadora", min_length=2, max_length=50)
    ram: int = Field(default="RAM de Computadora")
    almacenamiento: str = Field(default="Almacenamiento de Computadora", min_length=5, max_length=15)

    class Config:
        json_schema_extra = {
            "example": {
                "id": 10,
                "marca": "Dell",
                "modelo": "XPS 13",
                "color": "Plata",
                "ram": "16 GB",
                "almacenamiento": "512 GB SSD"
            }
        }

computadoras = [
    {
        "id": 10,
        "marca": "Dell",
        "modelo": "XPS 13",
        "color": "Plata",
        "ram": "16 GB",
        "almacenamiento": "512 GB SSD"
    },
    {
        "id": 20,
        "marca": "Acer",
        "modelo": "Swift 3",
        "color": "Azul",
        "ram": "8 GB",
        "almacenamiento": "256 GB SSD"
    },
    {
        "id": 30,
        "marca": "MSI",
        "modelo": "Prestige 14",
        "color": "Blanco",
        "ram": "32 GB",
        "almacenamiento": "1 TB SSD"
    },
    {
        "id": 40,
        "marca": "Dell",
        "modelo": "Inspiron 15",
        "color": "Negro",
        "ram": "8 GB",
        "almacenamiento": "1 TB HDD"
    },
    {
        "id": 50,
        "marca": "Samsung",
        "modelo": "Galaxy Book",
        "color": "Gris",
        "ram": "16 GB",
        "almacenamiento": "512 GB SSD"
    }
]

@app.get('/computadoras/', tags=['computadoras'], response_model=List[Computadoras])
def obtener_computadoras() -> List[Computadoras]:
    db = Session()
    computadoras = db.query(ComputadorasModel).all()
    return computadoras

@app.get('/computadoras/{id}', tags=['computadoras'], response_model=Computadoras)
def obtener_computadora_por_id(id: int) -> Computadoras:
    db = Session()
    computadora = db.query(ComputadorasModel).filter(ComputadorasModel.id == id).first()
    if not computadora:
        return JSONResponse(status_code=404, content={"message": "La computadora no fue encontrada"})
    return computadora

@app.post('/computadoras/', tags=['computadoras'], response_model=List[Computadoras], status_code=200)
def crear_computadora(computadora: Computadoras) -> dict:
    db = Session()
    nueva_computadora = ComputadorasModel(**computadora.dict())
    db.add(nueva_computadora)
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha registrado la computadora"})

@app.put('/computadoras/{id}', tags=['computadoras'], response_model=Computadoras, status_code=200)
def actualizar_computadora(id: int, computadora: Computadoras) -> dict:
    db = Session()
    resultado = db.query(ComputadorasModel).filter(ComputadorasModel.id == id).first()
    if not resultado:
        return JSONResponse(status_code=404, content={"message": "La computadora no fue encontrada"})
    resultado.marca = computadora.marca
    resultado.modelo = computadora.modelo
    resultado.color = computadora.color
    resultado.ram = computadora.ram
    resultado.almacenamiento = computadora.almacenamiento
    db.commit()
    return JSONResponse(status_code=200, content={"message": "La computadora ha sido actualizada"})

@app.delete('/computadoras/{id}', tags=['computadoras'], response_model=List[Computadoras], status_code=200)
def eliminar_computadora(id: int) -> dict:
    db = Session()
    resultado = db.query(ComputadorasModel).filter(ComputadorasModel.id == id).first()
    if not resultado:
        return JSONResponse(status_code=404, content={"message": "La computadora no fue encontrada"})
    db.delete(resultado)
    db.commit()
    return JSONResponse(status_code=200, content={"message": "La computadora ha sido eliminada"})
